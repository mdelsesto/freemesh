#!/bin/sh

echo "Content-type: text/html";
echo "";

p=`echo "$QUERY_STRING" | cut -d'=' -f2`;
router_key=`uci get wireless.mesh_five.key`;
m=`echo "$QUERY_STRING" | cut -d'=' -f1`;
mac=`echo "$QUERY_STRING" | cut -d'=' -f2 | cut -c 1-8`;
subnet=`uci get network.lan.ipaddr | sed 's/\.[0-9]*$/./'`;

# http://192.168.1.1/cgi-bin/handler.cgi?m=78:A3:51:XX:XX:XX:XX
if [ "$m" == "m" ] && [ "$mac" == "78:A3:51" ]; then
        echo "`uci get wireless.ap_two.ssid`,`uci get wireless.ap_two.key`,`uci get wireless.ap_five.ssid`,`uci get wireless.ap_five.key`,`uci get wireless.mesh_five.mesh_id`,`uci get wireless.mesh_five.key`,$subnet`uci get fm.router.next_static`";
# http://192.168.1.1/cgi-bin/handler.cgi?p=<router_key>
elif [ "$p" == "$router_key" ]; then
        echo "`uci get wireless.ap_two.ssid`,`uci get wireless.ap_two.key`,`uci get wireless.ap_five.ssid`,`uci get wireless.ap_five.key`";
elif [ "$m" == "i" ]; then
        p=$((p+1))
        uci set fm.router.next_static=$p;
        uci commit fm;
        echo "$p";
elif [ "$m" == "l" ]; then
		echo $p >> "/tmp/Logger";
else
        echo "go away";
fi
