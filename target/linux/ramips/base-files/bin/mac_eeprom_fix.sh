#!/bin/sh

EEPROM_DEV="/dev/$(grep factory /proc/mtd|awk -F: '{printf $1}')"
#EEPROM_DEV=./eeprom.bin
TMP_FILE="/tmp/factory_mac.bin";

STICKER_MAC="$(uci get ph.wiz.routerID)";
#STICKER_MAC="78:D3:51:3D:00:38";

get_mac() {
	local path="$1";
	local offset="$2";
	local mac="$(hexdump -v -n 6 -s $offset -e '6/1 "%02x"' $path 2>/dev/null)";
	echo $mac;
}

split_mac_hw(){
	local mac="$1";
	local retval="$(echo $mac|sed ':a;s/\B[0-9a-fA-F]\{6\}\>/\ &/;ta'|cut -d ' ' -f 2)";
	retval="$(echo $((0x$retval))|awk '{printf("%06x",$1)}')";
	echo $retval;
}

split_mac_oui(){
	local mac="$1";
	local retval="$(echo $mac|sed ':a;s/\B[0-9a-fA-F]\{6\}\>/\ &/;ta'|cut -d ' ' -f 1)";
	retval="$(echo $((0x$retval))|awk '{printf("%06x",$1)}')";
	echo $retval;
}

mac_hw_incr(){
	local mac_hw="$1";
	local retval="$(echo $((0x$mac_hw + 1))|awk '{printf("%06x",$1)}')";
	if [ "$((0x$retval))" -gt "$((0xffffff))" ];then
		retval="000000";
	fi
	echo $retval;
}
mac_hw_decr(){
	local mac_hw="$1";
	local retval="$(echo $((0x$mac_hw - 1))|awk '{printf("%06x",$1)}')";
	if [ "$((0x$retval))" -gt "$((0xffffff))" ];then
		retval="000000";
	fi
	echo $retval
}

write_mac(){
	local path="$1";
	local mac="$2";
	local offset="$3";
	for macbyte in $(echo $mac|sed ':a;s/\B[0-9 a-f]\{2\}\>/\ &/;ta'); do
		echo $((0x$macbyte))|awk '{printf("%c",$1)}'|dd of=$TMP_FILE conv=notrunc seek=$offset bs=1 count=1 2>/dev/null;
		offset="$(($offset + 1))";
	done;
}

boardname="$(cat /tmp/sysinfo/board_name)"
if [ "$boardname" != "zbt-we826-16M" -a "$boardname" != "zbt-we826-32M" ];then
	echo;
	echo "This script is for ZBT-WE826 use only.";
	echo;
	exit 1;
fi

if [ "x$STICKER_MAC" == "x" ];then
	echo;
	echo "Cannot find ph.wiz.routerID jey with uci to acquire MAC on sticker.";
	echo;
	exit 1;
fi

STICKER_MAC="$(echo $STICKER_MAC|sed 's/://g')";
STICKER_MAC_OUI="$(split_mac_oui $STICKER_MAC)";
STICKER_MAC_HW="$(split_mac_hw $STICKER_MAC)";
# little hack to do lowercase.
STICKER_MAC="$STICKER_MAC_OUI$STICKER_MAC_HW";

LAN_MAC="$(get_mac $EEPROM_DEV 40)";
if [ "$STICKER_MAC" == "$LAN_MAC" -a "$1" != "--force" ];then
	echo;
	echo "It seems that LAN ports MAC already conforms to sticker (uci get ph.wiz.routerID).";
	echo "You can override this with '--force' argument";
	echo;
	exit 1;
fi

if [ ! -e "/etc/backup/factory_mac.bin" ];then
	echo " Saving 'factory' partition to /etc/backup/factory_mac.bin.";
	if [ ! -d "/etc/backup" ];then mkdir -p /etc/backup;fi
	dd if=$EEPROM_DEV of=/etc/backup/factory_mac.bin 2>/dev/null;
	if [ -e "/etc/sysupgrade.conf" -a "$(grep '/etc/backup' /etc/sysupgrade.conf)" == "" ];then 
		echo "/etc/backup/" >> /etc/sysupgrade.conf;
	fi
else
	if [ "$1" != "--force" ];then
		echo "It seems you already have backup factory partition.";
		echo "You can continue execution without backup with '--force' argument,";
		echo " or move/remove file '/etc/backup/factory_mac.bin'.";
		exit 1;
	fi
fi

rm -f $TMP_FILE;
echo " Preparing new eeprom in $TMP_FILE.";
echo "   1. Copy eeprom from 'factory' partition.";
dd if=$EEPROM_DEV of=$TMP_FILE  2>/dev/null;

echo "   2. Fixing MAC addresses.";
WLAN5_MAC_HW="$(mac_hw_decr $STICKER_MAC_HW)";
WLAN2_MAC_HW="$(mac_hw_decr $WLAN5_MAC_HW)";
WAN_MAC_HW="$(mac_hw_incr $STICKER_MAC_HW)";

write_mac $TMP_FILE "$STICKER_MAC_OUI$WLAN2_MAC_HW" 4;
write_mac $TMP_FILE "$STICKER_MAC" 40;
write_mac $TMP_FILE "$STICKER_MAC_OUI$WAN_MAC_HW" 46;
write_mac $TMP_FILE "$STICKER_MAC_OUI$WLAN5_MAC_HW" $((0x8004));

echo " Writing $TMP_FILE to 'factory' partition.";
mtd write $TMP_FILE factory
rm $TMP_FILE;
echo " Done.";

exit 0;